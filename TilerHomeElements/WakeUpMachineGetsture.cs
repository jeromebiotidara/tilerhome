﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect.VisualGestureBuilder;
using Microsoft.Kinect;

namespace TilerHomeElements
{
    public class WakeUpMachineGetsture : GestureDetector
    {
        public WakeUpMachineGetsture(KinectSensor kinectSensor, GestureResultView gestureResultView) : base(kinectSensor, @"Database\WakeUpMachine.gbd", "WakeUpMachine")
        {
            if (gestureResultView == null)
            {
                throw new ArgumentNullException("gestureResultView");
            }
            this.vgbFrameSource.TrackingIdLost += this.Source_TrackingIdLost;
            this.GestureResultView = gestureResultView;
            // open the reader for the vgb frames

            this.vgbFrameReader = this.vgbFrameSource.OpenReader();
            if (this.vgbFrameReader != null)
            {
                this.vgbFrameReader.IsPaused = true;
                this.vgbFrameReader.FrameArrived += this.Reader_GestureFrameArrived;
            }

            // load the 'Seated' gesture from the gesture database
            using (VisualGestureBuilderDatabase database = new VisualGestureBuilderDatabase(this.gestureDatabase))
            {
                // we could load all available gestures in the database with a call to vgbFrameSource.AddGestures(database.AvailableGestures), 
                // but for this program, we only want to track one discrete gesture from the database, so we'll load it by name
                foreach (Gesture gesture in database.AvailableGestures)
                {
                    if (gesture.Name.Equals(this.seatedGestureName))
                    {
                        this.vgbFrameSource.AddGesture(gesture);
                    }
                }
            }
        }

        protected override void Reader_GestureFrameArrived(object sender, VisualGestureBuilderFrameArrivedEventArgs e)
        {
            VisualGestureBuilderFrameReference frameReference = e.FrameReference;
            using (VisualGestureBuilderFrame frame = frameReference.AcquireFrame())
            {
                if(frame != null)
                {
                    // get the discrete gesture results which arrived with the latest frame
                    IReadOnlyDictionary<Gesture, DiscreteGestureResult> discreteResults = frame.DiscreteGestureResults;

                    if (discreteResults != null)
                    {
                        // we only have one gesture in this source object, but you can get multiple gestures
                        foreach (Gesture gesture in this.vgbFrameSource.Gestures)
                        {
                            //if ((gesture.Name.Equals(this.seatedGestureName) ||gesture.Name.Equals(this.rightThumbsUpGestureName)|| gesture.Name.Equals(this.leftThumbsUpGestureName)) && gesture.GestureType == GestureType.Discrete)
                            if ((gesture.Name.Equals(this.seatedGestureName)) && gesture.GestureType == GestureType.Discrete)
                            {
                                DiscreteGestureResult result = null;
                                discreteResults.TryGetValue(gesture, out result);

                                if (result != null)
                                {
                                    // update the GestureResultView object with new gesture result values
                                    this.GestureResultView.UpdateGestureResult(true, result.Detected, result.Confidence);
                                    if (gestureDisplays != null)
                                    {
                                        foreach(GestureUIViewer GestureUIViewer in gestureDisplays[gesture.Name])
                                        {
                                            GestureUIViewer.updateGesture(result.Detected, gesture.Name);
                                        }
                                        //gestureDisplays[gesture.Name] for.updateGesture(result.Detected);
                                        //gestureDisplays.ForEach(obj => { obj.updateGesture(result.Detected); });
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
        }

        protected override void Source_TrackingIdLost(object sender, TrackingIdLostEventArgs e)
        {
            // update the GestureResultView object to show the 'Not Tracked' image in the UI
            this.GestureResultView.UpdateGestureResult(false, false, 0.0f);
        }
    }
}
