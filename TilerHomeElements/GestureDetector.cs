﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;
using Microsoft.Kinect.VisualGestureBuilder;

namespace TilerHomeElements
{
    public abstract class GestureDetector : IDisposable
    {
        /// <summary> Path to the gesture database that was trained with VGB </summary>
        protected readonly string gestureDatabase;
        protected readonly string seatedGestureName;

        /// <summary> Gesture frame reader which will handle gesture events coming from the sensor </summary>
        protected VisualGestureBuilderFrameReader vgbFrameReader = null;
        /// <summary> Gesture frame source which should be tied to a body tracking ID </summary>
        protected VisualGestureBuilderFrameSource vgbFrameSource = null;

        //data memeber is responsible for handling the rendering or processing the result of the gesture recognition
        public GestureResultView GestureResultView { get; protected set; }

        protected Dictionary<string, HashSet<GestureUIViewer>> gestureDisplays = new Dictionary<string, HashSet<GestureUIViewer>>();

        public GestureDetector(KinectSensor kinectSensor,  string gestureDatabase, string seatedGestureName, IEnumerable<GestureUIViewer> viewwers = null)
        {
            if (kinectSensor == null)
            {
                throw new ArgumentNullException("kinectSensor");
            }
            this.gestureDatabase = gestureDatabase;
            this.seatedGestureName = seatedGestureName;
            this.vgbFrameSource = new VisualGestureBuilderFrameSource(kinectSensor, 0);

            if (viewwers != null)
            {
                foreach(GestureUIViewer gestureUIViewer in viewwers)
                {
                    foreach(string viewId in gestureUIViewer.ViewIds)
                    {
                        if (gestureDisplays.ContainsKey(viewId))
                        {
                            gestureDisplays[viewId].Add(gestureUIViewer);
                        }
                        else
                        {
                            HashSet<GestureUIViewer> GestureUIViewers = new HashSet<GestureUIViewer>() { gestureUIViewer };
                            gestureDisplays.Add(viewId, GestureUIViewers);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Disposes the VisualGestureBuilderFrameSource and VisualGestureBuilderFrameReader objects
        /// </summary>
        /// <param name="disposing">True if Dispose was called directly, false if the GC handles the disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.vgbFrameReader != null)
                {
                    this.vgbFrameReader.FrameArrived -= this.Reader_GestureFrameArrived;
                    this.vgbFrameReader.Dispose();
                    this.vgbFrameReader = null;
                }

                if (this.vgbFrameSource != null)
                {
                    this.vgbFrameSource.TrackingIdLost -= this.Source_TrackingIdLost;
                    this.vgbFrameSource.Dispose();
                    this.vgbFrameSource = null;
                }
            }
        }

        abstract protected void Source_TrackingIdLost(object sender, TrackingIdLostEventArgs e);

        abstract protected void Reader_GestureFrameArrived(object sender, VisualGestureBuilderFrameArrivedEventArgs e);

        /// <summary>
        /// Gets or sets the body tracking ID associated with the current detector
        /// The tracking ID can change whenever a body comes in/out of scope
        /// </summary>
        public ulong TrackingId
        {
            get
            {
                return this.vgbFrameSource.TrackingId;
            }

            set
            {
                if (this.vgbFrameSource.TrackingId != value)
                {
                    this.vgbFrameSource.TrackingId = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the detector is currently paused
        /// If the body tracking ID associated with the detector is not valid, then the detector should be paused
        /// </summary>
        public bool IsPaused
        {
            get
            {
                return this.vgbFrameReader.IsPaused;
            }

            set
            {
                if (this.vgbFrameReader.IsPaused != value)
                {
                    this.vgbFrameReader.IsPaused = value;
                }
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
