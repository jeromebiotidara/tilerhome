﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;

namespace TilerHomeElements
{
    public abstract class GestureUIViewer : INotifyPropertyChanged
    {
        protected TimeSpan durationOfRecognition = new TimeSpan();
        protected string PatternName;
        protected TimeSpan continuousPositiveDetection = TimeSpan.FromMilliseconds(500);
        protected TimeSpan continuousNegativeDetection = TimeSpan.FromMilliseconds(300);
        protected string currentViewId = "";
        protected Dictionary<string, bool> PatternIsDetected = new Dictionary<string, bool>();
        protected Dictionary<string, DateTimeOffset> lastTrueDetection = new Dictionary<string, DateTimeOffset>();
        protected Dictionary<string, DateTimeOffset> lastFalseDetection = new Dictionary<string, DateTimeOffset>();
        protected Dictionary<string, DateTimeOffset> lastFlipTime = new Dictionary<string, DateTimeOffset>();
        protected Dictionary<string, DateTimeOffset> customSequence = new Dictionary<string, DateTimeOffset>();
        protected Dictionary<string, DateTimeOffset> customSequenceIni = new Dictionary<string, DateTimeOffset>();
        protected Dictionary<string, DateTimeOffset> customSequenceFalse = new Dictionary<string, DateTimeOffset>();
        protected Brush bodyColor = Brushes.Gray;

        private bool getFirstDetecedViewOption()
        {
            return PatternIsDetected.Values.First();
        }

        protected Func<bool> _processDetection;// = getFirstDetecedViewOption;


        
        String[] _ViewIds;

        public string Name
        {
            get
            {
                return this.PatternName;
            }
            protected set
            {
                this.PatternName = value;
            }
        }

        public virtual bool getDetectedFlag(string viewId)
        {
            return PatternIsDetected[viewId];
        }

        public virtual void setDetectedFlag(string viewId, bool value)
        {
            if (value)
            {
                lastTrueDetection[viewId] = DateTimeOffset.UtcNow;
            }
            else
            {
                lastFalseDetection[viewId] = DateTimeOffset.UtcNow;
                this.BodyColor = Brushes.Red;
            }

            if (processDetection())
            {
                this.BodyColor = Brushes.Purple;
            }
            else
            {
                this.BodyColor = Brushes.Brown;
            }

            if (value != this.PatternIsDetected[viewId])
            {
                setLastSwitchInStatus(viewId, DateTimeOffset.UtcNow);
            }
            this.PatternIsDetected[viewId] = value;
            currentViewId = viewId;
            this.NotifyPropertyChanged();
        }



        public String[] ViewIds {
            get {
                return _ViewIds;
            }
            set {
                _ViewIds = value;
                this.PatternIsDetected = this._ViewIds.ToDictionary(obj => obj, obj => false);
                this.lastTrueDetection = this._ViewIds.ToDictionary(obj => obj, obj => new DateTimeOffset());
                this.lastFalseDetection = this._ViewIds.ToDictionary(obj => obj, obj => new DateTimeOffset());
                this.lastFlipTime = this._ViewIds.ToDictionary(obj => obj, obj => new DateTimeOffset());
                currentViewId = _ViewIds.First();
            } }

        public DateTimeOffset getLastTrueDetection(string viewId)
        {
            return lastTrueDetection[viewId];
        }
        public virtual void setLastSwitchInStatus(string viewId, DateTimeOffset value)
        {
            //set
            {
                lastFlipTime[viewId] = value;
                this.NotifyPropertyChanged();
            }
        }

        public Func<bool> processDetection {
            get
            {
                if(_processDetection == null)
                {
                    return getFirstDetecedViewOption;
                }
                else
                {
                    return _processDetection;
                }
                
            }
            set
            {
                _processDetection = value;
            }
        }


        public Func<bool> tempDetectionHolder
        {
            get; set;
        }
        public void updateCustomeSequence(string sequence)
        {
            DateTimeOffset now = DateTimeOffset.UtcNow;
            if (customSequence.ContainsKey(sequence))
            {
                customSequence[sequence] = now;
            }
            else
            {
                customSequence.Add(sequence, now);
                customSequenceIni.Add(sequence, now);
            }
        }

        public virtual TimeSpan durationOfCurrentStatus
        {
            get
            {
                return getdurationOfCurrentStatus(currentViewId);
            }
            set
            {
                this.NotifyPropertyChanged();
            }
        }

        public virtual TimeSpan getdurationOfCurrentStatus(string viewId)
        {
                TimeSpan retValue = DateTimeOffset.UtcNow - lastFlipTime[viewId];
                return retValue;
        }


    protected virtual bool isSpikeIntervalSurpassed(DateTimeOffset lastKnownDetection, TimeSpan continuosExpectedSpan)
        {
            bool retValue = (DateTimeOffset.UtcNow - lastKnownDetection) > continuosExpectedSpan;
            return retValue;
        }

        public virtual void updateGesture(bool detected, string viewId)
        {
            if (this.getDetectedFlag(viewId))
            {
                if ((this.getDetectedFlag(viewId) != detected))
                {
                    bool isSurpassed = isSpikeIntervalSurpassed(lastTrueDetection[viewId], continuousPositiveDetection);
                    if (isSurpassed)
                    {
                        this.setDetectedFlag(viewId, detected);
                    }
                }
                else
                {
                    this.setDetectedFlag(viewId, detected);
                }
            }
            else
            {
                if ((this.getDetectedFlag(viewId) != detected))
                {
                    bool isSurpassed = isSpikeIntervalSurpassed(lastFalseDetection[viewId], continuousNegativeDetection);
                    if (isSurpassed)
                    {
                        this.setDetectedFlag(viewId, detected);
                    }
                }
                else
                {
                    this.setDetectedFlag(viewId, detected);
                }
            }

            durationOfCurrentStatus = continuousNegativeDetection;
        }


        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Notifies UI that a property has changed
        /// </summary>
        /// <param name="propertyName">Name of property that has changed</param> 
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary> 
        /// Gets the body color corresponding to the body index for the result
        /// </summary>
        public Brush BodyColor
        {
            get
            {
                return this.bodyColor;
            }

            private set
            {
                if (this.bodyColor != value)
                {
                    this.bodyColor = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

    }
}
