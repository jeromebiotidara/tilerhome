﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VideoSource;

namespace RGBMotionDetection
{
    public class Motiondetector
    {
        private IMotionDetector detector = new MotionDetector3Optimized();
        //private CameraWindow cameraWindow;
        private int statIndex = 0, statReady = 0;
        private System.Timers.Timer timer;
        private int intervalsToSave = 100;
        private System.Windows.Forms.MenuItem motionAlarmItem;
        //private AVIWriter writer = null;
        private bool saveOnMotion = true;
        private System.Windows.Forms.Panel panel;
        private Camera cameraDevice;
        public EventHandler onMotionDetected;
        public EventHandler onNoMotionDetected;
        public EventHandler onMotionDetectectionIsDisabled;
        private bool _Enabled = true;
        public bool isEnabled {
            get
            {
                return _Enabled;
            }
            set
            {
                detector.Enabled = value;
                _Enabled = value;
            }
        }
        public Motiondetector()
        {
            TimeSpan waitLagTime = new TimeSpan(0, 0, 1);
            detector.MotionLevelCalculation = true;
            this.motionAlarmItem = new System.Windows.Forms.MenuItem();
            this.motionAlarmItem.Checked = true;
            this.timer = new System.Timers.Timer();
            CaptureDeviceForm form = new CaptureDeviceForm();
            //this.cameraWindow = new CameraWindow();

            //this.cameraWindow.BackColor = System.Drawing.SystemColors.AppWorkspace;
            //this.cameraWindow.Camera = null;
            //this.cameraWindow.Location = new System.Drawing.Point(0, 0);
            //this.cameraWindow.Name = "cameraWindow";
            //this.cameraWindow.Size = new System.Drawing.Size(1920, 1080);
            //this.cameraWindow.TabIndex = 1;

            this.panel = new System.Windows.Forms.Panel();
            this.panel.SuspendLayout();
            //if (form.ShowDialog(this) == DialogResult.OK)
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // create video source
                CaptureDevice localSource = new CaptureDevice();
                localSource.VideoSource = form.Device;
                cameraDevice = new Camera(localSource, detector);
                // open it
                OpenVideoSource(localSource);
                SetMotionDetector();
            }

            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            //this.panel.Controls.Add( this.cameraWindow );
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.BackColor = System.Drawing.Color.Green;

            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1920, 1080);
            this.panel.TabIndex = 2;


            //System.Windows.Forms.Integration.WindowsFormsHost host1 = new System.Windows.Forms.Integration.WindowsFormsHost();
            //host1.Child = this.panel;
            //host1.RenderSize= new System.Windows.Size(700, 700);
            //host1.Background = 
            //InitializeComponent();
            //(this.TabItem2.Content as Grid).Children.Add(host1);
            this.panel.ResumeLayout(false);

            //this.Closed += new System.EventHandler(this.MainForm_Closing);
        }

        void MainForm_Closing(object sender, EventArgs e)
        {
            CloseFile();
        }

        public void cleanUp()
        {
            CloseFile();
        }
        private void SetMotionDetector()
        {
            //Camera camera = cameraWindow.Camera;
            Camera camera = cameraDevice;

            // enable/disable motion alarm
            if (detector != null)
            {
                detector.MotionLevelCalculation = motionAlarmItem.Checked;
            }

            // set motion detector to camera
            if (camera != null)
            {
                camera.Lock();
                camera.MotionDetector = detector;

                // reset statistics
                statIndex = statReady = 0;
                camera.Unlock();
            }
        }

        private void OpenVideoSource(IVideoSource source)
        {
            // set busy cursor
            //SafeFileHandle panHandle = new SafeFileHandle(System.Windows.Forms.Cursors.PanNorth.Handle, false);
            //this.Cursor = System.Windows.Interop.CursorInteropHelper.Create(panHandle);

            //this.Cursor = System.Windows.Input.Cursors.Wait;

            // close previous file
            CloseFile();

            // enable/disable motion alarm
            if (detector != null)
            {
                detector.MotionLevelCalculation = motionAlarmItem.Checked;
            }

            // create camera
            Camera camera = new Camera(source, detector);
            // start camera
            camera.Start();

            // attach camera to camera window
            //cameraWindow.Camera = camera;

            // reset statistics
            statIndex = statReady = 0;

            // set event handlers
            camera.NewFrame += new EventHandler(camera_NewFrame);
            //camera.NoProcessing += new EventHandler(camera_NewFrame);
            camera.Alarm += new EventHandler(camera_Alarm);
            camera.NoAlarm += new EventHandler(camera_NoAlarm);

            // start timer
            timer.Start();

            //this.Cursor = System.Windows.Input.Cursors.Arrow;
        }

        private void camera_Alarm(object sender, System.EventArgs e)
        {
            // save movie for 5 seconds after motion stops
            intervalsToSave = (int)(5 * (1000 / timer.Interval));
            if (isEnabled)
            {
                onMotionDetected(this, new EventArgs());
            }
            else
            {
                onMotionDetectectionIsDisabled(this, new EventArgs());
            }
            
            //Dispatcher.BeginInvoke(DispatcherPriority.Input, new ThreadStart(() =>
            //{
            //    this.DetectionStatus.Text = "True";
            //    this.panel.BackColor = System.Drawing.Color.Red;
            //}));
        }

        


        private void camera_NoAlarm(object sender, System.EventArgs e)
        {
            // save movie for 5 seconds after motion stops
            intervalsToSave = (int)(5 * (1000 / timer.Interval));
            onNoMotionDetected(this, new EventArgs());
            //Dispatcher.BeginInvoke(DispatcherPriority.Input, new ThreadStart(() =>
            //{
            //    this.DetectionStatus.Text = "False";
            //    this.panel.BackColor = System.Drawing.Color.Green;
            //}));


        }

        private void camera_NewFrame(object sender, System.EventArgs e)
        {
            if (!detector.Enabled)
            {
                onMotionDetectectionIsDisabled(this, new EventArgs());
            }
            if ((intervalsToSave != 0) && (saveOnMotion == true) && false)
            {
                // lets save the frame
                //if (writer == null)
                //{
                //    // create file name
                //    DateTime date = DateTime.Now;
                //    String fileName = String.Format("{0}-{1}-{2} {3}-{4}-{5}.avi",
                //        date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second);

                //    try
                //    {
                //        // create AVI writer
                //        writer = new AVIWriter("wmv3");
                //        // open AVI file
                //        //writer.Open(fileName, cameraWindow.Camera.Width, cameraWindow.Camera.Height);
                //        writer.Open(fileName, cameraDevice.Width, cameraDevice.Height);
                //    }
                //    catch (ApplicationException ex)
                //    {
                //        if (writer != null)
                //        {
                //            writer.Dispose();
                //            writer = null;
                //        }
                //    }
                //}

                //// save the frame
                //Camera camera = cameraDevice;// cameraWindow.Camera;

                //camera.Lock();
                //writer.AddFrame(camera.LastFrame);
                //camera.Unlock();
            }
        }

        private void CloseFile()
        {
            //Camera camera = cameraWindow.Camera;
            Camera camera = cameraDevice;

            if (camera != null)
            {
                // detach camera from camera window
                //cameraWindow.Camera = null;

                // signal camera to stop
                camera.SignalToStop();
                // wait for the camera
                camera.WaitForStop();

                camera = null;

                if (detector != null)
                    detector.Reset();
            }

            //if (writer != null)
            //{
            //    writer.Dispose();
            //    writer = null;
            //}
            intervalsToSave = 0;
        }
    }
}
