﻿//------------------------------------------------------------------------------
// <copyright file="GestureDetector.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace TilerHome
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Kinect;
    using Microsoft.Kinect.VisualGestureBuilder;
    using TilerHomeElements;

    /// <summary>
    /// Gesture Detector class which listens for VisualGestureBuilderFrame events from the service
    /// and updates the associated GestureResultView object with the latest results for the 'Seated' gesture
    /// </summary>
    public class GestureDetectorIni : GestureDetector
    {
        /// <summary> Path to the gesture database that was trained with VGB </summary>
        //private readonly string gestureDatabase = @"Database\Seateds.gbd";

        /// <summary> Name of the discrete gesture in the database that we want to track </summary>
        //private readonly string seatedGestureName = "Seated";

        private readonly string leftThumbsUpGestureName = "leftThumbsSeatingDownUP_Left";
        private readonly string rightThumbsUpGestureName = "leftThumbsSeatingDownUP_Right";

        private TimeSpan seatingDuration = new TimeSpan();
        private DateTimeOffset beforeTime = DateTimeOffset.Now.AddTicks(-1);
        private DateTimeOffset lastTrueDetected = DateTimeOffset.Now.AddTicks(-1);

        /// <summary> Gesture frame source which should be tied to a body tracking ID </summary>
        //private VisualGestureBuilderFrameSource vgbFrameSource = null;

        /// <summary> Gesture frame reader which will handle gesture events coming from the sensor </summary>
        //private VisualGestureBuilderFrameReader vgbFrameReader = null;

        /// <summary>
        /// Initializes a new instance of the GestureDetector class along with the gesture frame source and reader
        /// </summary>
        /// <param name="kinectSensor">Active sensor to initialize the VisualGestureBuilderFrameSource object with</param>
        /// <param name="gestureResultView">GestureResultView object to store gesture results of a single body to</param>
        public GestureDetectorIni(KinectSensor kinectSensor, GestureResultViewIni gestureResultView, IEnumerable<GestureUIViewer> viewwers = null) : base(kinectSensor, @"Database\WakeUpMachine.gbd,Database\Seateds.gbd", "WakeUpMachine,Seated,WakingUpFromBed,StandingOrWalking", viewwers)//@"Database\Seateds.gbd, "Seated")
        {
            if (kinectSensor == null)
            {
                throw new ArgumentNullException("kinectSensor");
            }

            if (gestureResultView == null)
            {
                throw new ArgumentNullException("gestureResultView");
            }

            this.GestureResultView = gestureResultView;

            // create the vgb source. The associated body tracking ID will be set when a valid body frame arrives from the sensor.
            this.vgbFrameSource = new VisualGestureBuilderFrameSource(kinectSensor, 0);
            this.vgbFrameSource.TrackingIdLost += this.Source_TrackingIdLost;

            // open the reader for the vgb frames
            this.vgbFrameReader = this.vgbFrameSource.OpenReader();
            if (this.vgbFrameReader != null)
            {
                this.vgbFrameReader.IsPaused = true;
                this.vgbFrameReader.FrameArrived += this.Reader_GestureFrameArrived;
            }
            char[] separator = new char[] { ',' };
            String[] subFiles = (this.gestureDatabase.Split(','));

            foreach (String eachFile in subFiles)
            {
                // load the 'Seated' gesture from the gesture database
                using (VisualGestureBuilderDatabase database = new VisualGestureBuilderDatabase(eachFile))
                {
                    // we could load all available gestures in the database with a call to vgbFrameSource.AddGestures(database.AvailableGestures), 
                    // but for this program, we only want to track one discrete gesture from the database, so we'll load it by name
                    foreach (Gesture gesture in database.AvailableGestures)
                    {
                        if (this.seatedGestureName.Contains(gesture.Name))
                        {
                            this.vgbFrameSource.AddGesture(gesture);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handles gesture detection results arriving from the sensor for the associated body tracking Id
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        protected override void Reader_GestureFrameArrived(object sender, VisualGestureBuilderFrameArrivedEventArgs e)
        {
            VisualGestureBuilderFrameReference frameReference = e.FrameReference;
            using (VisualGestureBuilderFrame frame = frameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    // get the discrete gesture results which arrived with the latest frame
                    IReadOnlyDictionary<Gesture, DiscreteGestureResult> discreteResults = frame.DiscreteGestureResults;

                    if (discreteResults != null)
                    {
                        // we only have one gesture in this source object, but you can get multiple gestures
                        foreach (Gesture gesture in this.vgbFrameSource.Gestures)
                        {
                            //if ((gesture.Name.Equals(this.seatedGestureName) ||gesture.Name.Equals(this.rightThumbsUpGestureName)|| gesture.Name.Equals(this.leftThumbsUpGestureName)) && gesture.GestureType == GestureType.Discrete)
                            //if ((gesture.Name.Equals(this.seatedGestureName)) && gesture.GestureType == GestureType.Discrete)
                            if((this.seatedGestureName.Contains(gesture.Name)) && gesture.GestureType == GestureType.Discrete)
                            {
                                DiscreteGestureResult result = null;
                                discreteResults.TryGetValue(gesture, out result);

                                if (result != null)
                                {
                                    // update the GestureResultView object with new gesture result values
                                    assessSeating(true, result.Detected, result.Confidence);
                                    this.GestureResultView.UpdateGestureResult(true, result.Detected, result.Confidence);//, seatingDuration);
                                    if (gestureDisplays != null)
                                    {
                                        if(gestureDisplays.ContainsKey(gesture.Name))
                                        {
                                            foreach (GestureUIViewer GestureUIViewer in gestureDisplays[gesture.Name])
                                            {
                                                GestureUIViewer.updateGesture(result.Detected, gesture.Name);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        void assessSeating(bool stillInFrame, bool isUSerSeated, float confidence)
        {
            if (!stillInFrame || !isUSerSeated)
            {
                lastTrueDetected = beforeTime;
            }
            evaluateSeatingDuration();
        }

        void evaluateSeatingDuration()
        {
            if (lastTrueDetected != beforeTime)
            {
                seatingDuration = DateTimeOffset.Now - lastTrueDetected;
            }
            else
            {
                lastTrueDetected = DateTimeOffset.Now;
                seatingDuration = new TimeSpan();
            }
        }

        /// <summary>
        /// Handles the TrackingIdLost event for the VisualGestureBuilderSource object
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        protected override void Source_TrackingIdLost(object sender, TrackingIdLostEventArgs e)
        {
            assessSeating(false, false, 0.0f);
            // update the GestureResultView object to show the 'Not Tracked' image in the UI
            this.GestureResultView.UpdateGestureResult(false, false, 0.0f);//, seatingDuration);
        }
    }
}
