﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using System.ComponentModel;
using TilerHomeElements;
using RGBMotionDetection;
using System.Windows.Threading;
using System.Threading;

namespace TilerHome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary> Active Kinect sensor </summary>
        private KinectSensor kinectSensor = null;
        NShet VarGis;// = new NShet();
        public NShet.KinectManager KinectManager;
        /// <summary> Array for the bodies (Kinect will track up to 6 people simultaneously) </summary>
        private Body[] bodies = null;
        /// <summary> Reader for body frames </summary>
        private BodyFrameReader bodyFrameReader = null;

        /// <summary> Current status text to display </summary>
        private string statusText = null;

        /// <summary> KinectBodyView object which handles drawing the Kinect bodies to a View box in the UI </summary>
        private KinectBodyView kinectBodyView = null;

        /// <summary> List of gesture detectors, there will be one detector created for each potential body (max of 6) </summary>
        private List<GestureDetector> gestureDetectorList = null;
        /// <summary> List of gesture detectors, there will be one detector created for each potential body (max of 6) </summary>
        private List<GestureUIViewer> gestureUIViewers= null;
        private Motiondetector rgbMotionDetector = null;
        DateTimeOffset beginningOfTime = new DateTimeOffset();
        List<ContentControl> CurrentContentControls = new List<ContentControl>();

        public void BindWithKinectSensorChange(object sender, EventArgs e)
        {
            kinectSensor = KinectManager.Sensor;
            this.CurrentRoom.Text = "Current Room is " + KinectManager.CurrentRoom;
        }

        void InitializeKinectParams()
        {
            kinectSensor = KinectManager.Sensor;
            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;

            // open the sensor
            this.kinectSensor.Open();

            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.NoSensorStatusText;

            // open the reader for the body frames
            this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();

            this.bodyFrameReader.FrameArrived += this.Reader_BodyFrameArrived;
        }

        void InitializeGestureFrames() {
            if (this.contentGrid == null)
            {
                return;
            }
            CurrentContentControls.ForEach(obj => this.contentGrid.Children.Remove(obj));
            this.gestureDetectorList = new List<GestureDetector>();
            GestureUIViewer wakeUP = new WakingUpGestureViewer("Wake Up Machine");
            wakeUP.ViewIds = new string[] { "WakeUpMachine" };
            GestureUIViewer seated = new BasicGestureViewer("Seated user");
            seated.ViewIds = new string[] { "Seated" };
            GestureUIViewer lyingDown = new BasicGestureViewer("Laying down User");
            lyingDown.ViewIds = new string[] { "WakingUpFromBed" };
            GestureUIViewer StandingOrWalking = new BasicGestureViewer("Standing user");
            StandingOrWalking.ViewIds = new string[] { "StandingOrWalking" };

            GestureUIViewer StandingOrWalkingThenSleeping = new BasicGestureViewer("Sleeping");
            StandingOrWalkingThenSleeping.ViewIds = new string[] { "StandingOrWalking", "WakingUpFromBed" };
            StandingOrWalkingThenSleeping.processDetection = (() => {
                int seconds = 5;
                if (StandingOrWalkingThenSleeping.getDetectedFlag("WakingUpFromBed"))
                {
                    DateTimeOffset lastStandingTime = StandingOrWalkingThenSleeping.getLastTrueDetection("StandingOrWalking");
                    DateTimeOffset lastLayDownTime = StandingOrWalkingThenSleeping.getLastTrueDetection("WakingUpFromBed");
                    if ((lastStandingTime < lastLayDownTime) && (lastStandingTime != beginningOfTime))
                    {
                        return true;
                    }
                }
                return false;
            });


            GestureUIViewer SleepThenAwake = new BasicGestureViewer("Sleeping Then Awake");
            SleepThenAwake.ViewIds = new string[] { "StandingOrWalking", "WakingUpFromBed" };
            string sleepBeginId = Guid.NewGuid().ToString();
            string WakeId = Guid.NewGuid().ToString();
            Func<bool> walkingThenSleeping = (() => {
                int seconds = 5;
                if (SleepThenAwake.getDetectedFlag("WakingUpFromBed"))
                {
                    DateTimeOffset lastStandingTime = StandingOrWalkingThenSleeping.getLastTrueDetection("StandingOrWalking");
                    DateTimeOffset lastLayDownTime = StandingOrWalkingThenSleeping.getLastTrueDetection("WakingUpFromBed");
                    if ((lastStandingTime < lastLayDownTime) && (lastStandingTime != beginningOfTime))
                    {
                        Func<bool> holder = SleepThenAwake.processDetection;
                        SleepThenAwake.processDetection = SleepThenAwake.tempDetectionHolder;
                        SleepThenAwake.tempDetectionHolder = holder;
                        SleepThenAwake.updateCustomeSequence(sleepBeginId);
                        return false;
                    }
                }
                return false;
            });
            Func<bool>  SleepingThenAwake = (() => {
                int seconds = 5;
                if (SleepThenAwake.getDetectedFlag("StandingOrWalking"))
                {
                    DateTimeOffset lastStandingTime = StandingOrWalkingThenSleeping.getLastTrueDetection("StandingOrWalking");
                    DateTimeOffset lastLayDownTime = StandingOrWalkingThenSleeping.getLastTrueDetection("WakingUpFromBed");
                    if ((lastStandingTime > lastLayDownTime) && (lastLayDownTime != beginningOfTime))
                    {
                        System.Media.SoundPlayer player = new System.Media.SoundPlayer();

                        player.SoundLocation = @"C:\Users\jerom\Downloads\startup_18.wav";
                        player.Play();
                        Func<bool> holder = SleepThenAwake.processDetection;
                        SleepThenAwake.processDetection = SleepThenAwake.tempDetectionHolder;
                        SleepThenAwake.tempDetectionHolder = holder;
                        SleepThenAwake.updateCustomeSequence(WakeId);
                        return true;
                    }
                }
                return false;
            });

            SleepThenAwake.processDetection = walkingThenSleeping;
            SleepThenAwake.tempDetectionHolder = SleepingThenAwake;
            gestureUIViewers = new List<GestureUIViewer>();
            gestureUIViewers.Add(wakeUP);
            gestureUIViewers.Add(seated);
            gestureUIViewers.Add(lyingDown);
            gestureUIViewers.Add(StandingOrWalking);
            gestureUIViewers.Add(StandingOrWalkingThenSleeping);
            gestureUIViewers.Add(SleepThenAwake);
            int col0Row = 0;
            int col1Row = 0;
            int maxBodies = this.gestureUIViewers.Count;
            for (int i = 0; i < maxBodies; ++i)
            {
                // split gesture results across the first two columns of the content grid
                ContentControl contentControl = new ContentControl();
                contentControl.Content = this.gestureUIViewers[i];

                if (i % 2 == 0)
                {
                    // Gesture results for bodies: 0, 2, 4
                    Grid.SetColumn(contentControl, 0);
                    Grid.SetRow(contentControl, col0Row);
                    ++col0Row;
                }
                else
                {
                    // Gesture results for bodies: 1, 3, 5
                    Grid.SetColumn(contentControl, 1);
                    Grid.SetRow(contentControl, col1Row);
                    ++col1Row;
                }
                this.contentGrid.Children.Add(contentControl);
                CurrentContentControls.Add(contentControl);
            }


            maxBodies = this.kinectSensor.BodyFrameSource.BodyCount;
            for (int i = 0; i < maxBodies; ++i)
            {
                //WakingUpResult result = new WakingUpResult(i, false, false, 0.0f);
                //GestureDetector detector = new WakeUpMachineGetsture(this.kinectSensor, result);
                GestureResultViewIni result = new GestureResultViewIni(i, false, false, 0.0f);
                GestureDetector detector = new GestureDetectorIni(this.kinectSensor, result, gestureUIViewers);
                this.gestureDetectorList.Add(detector);

                // split gesture results across the first two columns of the content grid
                ContentControl contentControl = new ContentControl();
                contentControl.Content = this.gestureDetectorList[i].GestureResultView;

                if (i % 2 == 0)
                {
                    // Gesture results for bodies: 0, 2, 4
                    Grid.SetColumn(contentControl, 0);
                    Grid.SetRow(contentControl, col0Row);
                    ++col0Row;
                }
                else
                {
                    // Gesture results for bodies: 1, 3, 5
                    Grid.SetColumn(contentControl, 1);
                    Grid.SetRow(contentControl, col1Row);
                    ++col1Row;
                }

                this.contentGrid.Children.Add(contentControl);
                CurrentContentControls.Add(contentControl);
            }

            this.CurrentRoom.Text = "Current Room is " + KinectManager.CurrentRoom;
        }

        public MainWindow()
        {
            VarGis = new NShet();
            KinectManager = VarGis.KinectSensorManager;
            KinectManager.KinectSensorChange += BindWithKinectSensorChange;
            this.kinectSensor = KinectManager.Sensor;
            InitializeKinectParams();
            ///*
            this.rgbMotionDetector = VarGis.rgbCameraDetector;
            this.rgbMotionDetector.onMotionDetected += ((object sender, System.EventArgs e) => {
                Dispatcher.BeginInvoke(DispatcherPriority.Input, new ThreadStart(() =>
                {
                    this.contentGrid.Background = Brushes.Red;
                    //this.DetectionStatus.Text = "False";
                    //this.panel.BackColor = System.Drawing.Color.Green;
                }));

                
            });
            this.rgbMotionDetector.onNoMotionDetected += ((object sender, System.EventArgs e) => {
                
                Dispatcher.BeginInvoke(DispatcherPriority.Input, new ThreadStart(() =>
                {
                    this.contentGrid.Background = Brushes.Green;
                }));
            });
            this.rgbMotionDetector.onMotionDetectectionIsDisabled += ((object sender, System.EventArgs e) => {

                Dispatcher.BeginInvoke(DispatcherPriority.Input, new ThreadStart(() =>
                {
                    this.contentGrid.Background = Brushes.Violet;
                }));
            });
            //*/
            // only one sensor is currently supported
            //this.kinectSensor = KinectSensor.GetDefault();

            // set IsAvailableChanged event notifier
            ///*
            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;

            // open the sensor
            this.kinectSensor.Open();

            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.NoSensorStatusText;

            // open the reader for the body frames
            this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();

            // set the BodyFramedArrived event notifier
            this.bodyFrameReader.FrameArrived += this.Reader_BodyFrameArrived;
            //*/

            // initialize the BodyViewer object for displaying tracked bodies in the UI
            this.kinectBodyView = new KinectBodyView(KinectManager);

            // initialize the gesture detection objects for our gestures
            this.gestureDetectorList = new List<GestureDetector>();

            InitializeComponent();

            // set our data context objects for display in UI
            this.DataContext = this;
            this.kinectBodyViewbox.DataContext = this.kinectBodyView;
            InitializeGestureFrames();
        }

        #region properties
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Handles the event when the sensor becomes unavailable (e.g. paused, closed, unplugged).
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            // on failure, set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.SensorNotAvailableStatusText;
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.bodyFrameReader != null)
            {
                // BodyFrameReader is IDisposable
                this.bodyFrameReader.FrameArrived -= this.Reader_BodyFrameArrived;
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }

            if (this.gestureDetectorList != null)
            {
                // The GestureDetector contains disposable members (VisualGestureBuilderFrameSource and VisualGestureBuilderFrameReader)
                foreach (GestureDetector detector in this.gestureDetectorList)
                {
                    detector.Dispose();
                }

                this.gestureDetectorList.Clear();
                this.gestureDetectorList = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.IsAvailableChanged -= this.Sensor_IsAvailableChanged;
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }

            VarGis.Close();
        }

        /// <summary>
        /// Handles the body frame data arriving from the sensor and updates the associated gesture detector object for each body
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_BodyFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {

            bool dataReceived = false;

            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    if (this.bodies == null)
                    {
                        // creates an array of 6 bodies, which is the max number of bodies that Kinect can track simultaneously
                        this.bodies = new Body[bodyFrame.BodyCount];
                    }

                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                    // As long as those body objects are not disposed and not set to null in the array,
                    // those body objects will be re-used.
                    bodyFrame.GetAndRefreshBodyData(this.bodies);
                    dataReceived = true;
                }
            }

            if (dataReceived)
            {
                // visualize the new body data
                this.kinectBodyView.UpdateBodyFrame(this.bodies);

                // we may have lost/acquired bodies, so update the corresponding gesture detectors
                if (this.bodies != null)
                {
                    // loop through all bodies to see if any of the gesture detectors need to be updated
                    //int maxBodies = this.gestureDetectorList.Count; ;
                    int maxBodies = this.kinectSensor.BodyFrameSource.BodyCount;
                    for (int i = 0; i < maxBodies; ++i)
                    {
                        Body body = this.bodies[i];
                        ulong trackingId = body.TrackingId;

                        // if the current body TrackingId changed, update the corresponding gesture detector with the new value
                        if (trackingId != this.gestureDetectorList[i].TrackingId)
                        {
                            this.gestureDetectorList[i].TrackingId = trackingId;

                            // if the current body is tracked, unpause its detector to get VisualGestureBuilderFrameArrived events
                            // if the current body is not tracked, pause its detector so we don't waste resources trying to get invalid gesture results
                            this.gestureDetectorList[i].IsPaused = trackingId == 0;
                        }
                    }
                }
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            KinectManager.switchRooms();
            this.SwitchButton.Content = KinectManager.CurrentRoom.ToString();
        }
    }
}
