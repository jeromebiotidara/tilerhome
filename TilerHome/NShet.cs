﻿using Microsoft.Kinect;
using RGBMotionDetection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TilerHomeElements;
using static TilerHomeElements.TilerKinectHardwareManager;

namespace TilerHome
{
    public class NShet
    {
        const int NumberOfKinecSensor = 2;
        public static readonly KinectRooms RGBCameraRoom = KinectRooms.LivingRoom;
        private Motiondetector rgbMotionDetector = null;
        string Name = "Vargis";
        DateTimeOffset DOC = new DateTimeOffset(2016, 5, 16, 0, 0, 0, new TimeSpan());
        KinectManager _KinectSensorManager;
        private BodyFrameReader bodyFrameReader = null;
        private Body[] bodies = null;
        public NShet()
        {
            _KinectSensorManager = new NShet.KinectManager();
            rgbMotionDetector = new Motiondetector();
            Initialize();
            _KinectSensorManager.KinectSensorChange += BindWithKinectSensorChange;
            this.bodyFrameReader = KinectSensorManager.Sensor.BodyFrameSource.OpenReader();
            this.bodyFrameReader.FrameArrived += this.ReinitializeTheBodyFrameEnrollment;
        }

        void Initialize()
        {
            subscribeToMotionDetectionInRgb();
            _KinectSensorManager.switchToBedRoomSensor();
        }

        void subscribeToMotionDetectionInRgb()
        {
            this.rgbMotionDetector.onMotionDetected += RgbMotionDetected;

            this.rgbMotionDetector.onNoMotionDetected += RgbMotionNotDetected;
        }

        void RgbMotionDetected(object sender, System.EventArgs e)
        {
            _KinectSensorManager.switchToLivingRoomSensor();
        }

        void RgbMotionNotDetected(object sender, System.EventArgs e)
        {
            _KinectSensorManager.switchToBedRoomSensor();
        }

        void BindWithKinectSensorChange(object sender, EventArgs e)
        {
            if(KinectSensorManager.CurrentRoom == RGBCameraRoom)
            {
                rgbMotionDetector.isEnabled = false;
            }
            else
            {
                rgbMotionDetector.isEnabled = true;
            }

        }

        public void ReinitializeTheBodyFrameEnrollment(object sender, BodyFrameArrivedEventArgs e)
        {
            bool dataReceived = false;

            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    if (this.bodies == null)
                    {
                        // creates an array of 6 bodies, which is the max number of bodies that Kinect can track simultaneously
                        this.bodies = new Body[bodyFrame.BodyCount];
                    }

                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                    // As long as those body objects are not disposed and not set to null in the array,
                    // those body objects will be re-used.
                    bodyFrame.GetAndRefreshBodyData(this.bodies);
                    dataReceived = true;
                }
            }

            bool foundABody = false;
            if (dataReceived)
            {
                int maxBodies = this.KinectSensorManager.Sensor.BodyFrameSource.BodyCount;
                // we may have lost/acquired bodies, so update the corresponding gesture detectors
                if ((this.bodies != null)  && this.KinectSensorManager.Sensor.BodyFrameSource.BodyCount>0)
                {
                    //rgbMotionDetector.isEnabled = true;
                    
                    for (int i = 0; i < maxBodies; ++i)
                    {
                        Body body = this.bodies[i];
                        ulong trackingId = body.TrackingId;
                        if (body.IsTracked)
                        {
                            foundABody = true;
                            break;
                        }
                        //// if the current body TrackingId changed, update the corresponding gesture detector with the new value
                        //if (trackingId != this.gestureDetectorList[i].TrackingId)
                        //{
                        //    this.gestureDetectorList[i].TrackingId = trackingId;

                        //    // if the current body is tracked, unpause its detector to get VisualGestureBuilderFrameArrived events
                        //    // if the current body is not tracked, pause its detector so we don't waste resources trying to get invalid gesture results
                        //    this.gestureDetectorList[i].IsPaused = trackingId == 0;
                        //}
                    }
                    if (foundABody)
                    {
                        rgbMotionDetector.isEnabled = false;
                    }
                }
            }
            if(!foundABody)
            {
                rgbMotionDetector.isEnabled = true;
            }
        }

        public void Close()
        {
            this.bodyFrameReader.FrameArrived -= this.ReinitializeTheBodyFrameEnrollment;
            _KinectSensorManager.Close();
            rgbMotionDetector.cleanUp();
        }

        public KinectSensor Kinect
        {
            get
            {
                return _KinectSensorManager.Sensor; ;
            }
        }


        public Motiondetector rgbCameraDetector
        {
            get
            {
                return rgbMotionDetector;
            }
        }
        public KinectManager KinectSensorManager
        {
            get
            {
                return _KinectSensorManager;
            }
        }

        public class KinectManager
        {
            bool SwitchIsComplete = true;
            const int threadaSleepInMs = 500;
            KinectRooms _CurrentRoom = KinectRooms.LivingRoom;
            KinectSensor currentSensor;
            private HashSet<TilerKinectHardwareManager.KinectRooms> kinectSensors = new HashSet<TilerKinectHardwareManager.KinectRooms>() {
                TilerKinectHardwareManager.KinectRooms.BedRoom,
                TilerKinectHardwareManager.KinectRooms.LivingRoom
            };

            Dictionary<KinectRooms, Action> kinectRoomFuncs;

            /// <summary>
            /// This triggered when a kinect sensor change has occured
            /// </summary>
            public EventHandler KinectSensorChange;
            public KinectManager()
            {
                currentSensor = KinectSensor.GetDefault();
                kinectRoomFuncs = new Dictionary<KinectRooms, Action>() {
                    { TilerKinectHardwareManager.KinectRooms.BedRoom, ()=> { switchToBedRoomSensor(); }},
                    { TilerKinectHardwareManager.KinectRooms.LivingRoom, ()=> { switchToLivingRoomSensor(); }}
                };
            }

            public KinectSensor Sensor
            {
                get
                {
                    return currentSensor;
                }
            }

            public void OpenSensor()
            {
                if (currentSensor != null)
                {
                    currentSensor.Open();
                }
            }
            public void Close()
            {
                CloseSensor();
            }
            public void CloseSensor()
            {
                if (currentSensor != null)
                {
                    currentSensor.Close();
                }
            }

            public void switchToBedRoomSensor()
            {
                if((_CurrentRoom != KinectRooms.BedRoom ) && SwitchIsComplete)
                {
                    SwitchIsComplete = false;
                    CloseSensor();
                    TilerKinectHardwareManager.EnableBedRoomKinect();
                    Thread.Sleep(threadaSleepInMs);
                    currentSensor = KinectSensor.GetDefault();
                    OpenSensor();
                    _CurrentRoom = KinectRooms.BedRoom;
                    SwitchIsComplete = true;
                    if (KinectSensorChange != null)
                    {
                        KinectSensorChange(this, new EventArgs());
                    }
                }
            }

            public void switchToLivingRoomSensor()
            {
                if ((_CurrentRoom != KinectRooms.LivingRoom)&& SwitchIsComplete)
                {
                    SwitchIsComplete = false;
                    CloseSensor();
                    TilerKinectHardwareManager.EnableLivingRoomKinect();
                    Thread.Sleep(threadaSleepInMs);
                    currentSensor = KinectSensor.GetDefault();
                    OpenSensor();
                    _CurrentRoom = KinectRooms.LivingRoom;
                    SwitchIsComplete = true;
                    if (KinectSensorChange != null)
                    {
                        KinectSensorChange(this, new EventArgs());
                    }
                }
            }

            bool IsAvailable { get; set; }

            public KinectRooms CurrentRoom
            {
                get
                {
                    return _CurrentRoom; ;
                }
            }

            public void switchRooms()
            {
                KinectRooms nextRoom = kinectSensors.First();
                bool breakNext = false;
                foreach(KinectRooms eachKinectRoom in kinectSensors)
                {
                    if (breakNext)
                    {
                        nextRoom = eachKinectRoom;
                        break;
                    }
                    if (eachKinectRoom == _CurrentRoom)
                    {
                        breakNext = true;
                    }
                }
                kinectRoomFuncs[nextRoom]();
            }

            public bool isBusy
            {
                get
                {
                    return !SwitchIsComplete;
                }
            }
            
        }
    }
}
